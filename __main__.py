import functools
import operator
from collections import Counter
from itertools import combinations

import matplotlib.pyplot as plt

from . import *


def create_players(num: int) -> list:
    return [strategies[x % len(strategies)]() for x in range(num)]


def play_game(n: int, m: int, p: int, k: int):
    contestants = create_players(n)
    total_payout_data = {"generation": [], "players": {"Defector": [], "Cooperator": [], "Grudger": [], "Tit For Tat": []}}
    population_percent_data = {"generation": [], "players": {"Defector": [], "Cooperator": [], "Grudger": [], "Tit For Tat": []}}
    average_payout_data = {"generation": [], "players": {"Defector": [], "Cooperator": [], "Grudger": [], "Tit For Tat": []}}
    for generation in range(k):
        for combo in combinations(contestants, 2):
            try:
                [combo for i in combo]
            except AttributeError:
                return print(combo)
            for _ in range(m):
                combo[0].play(combo[1])
        contestants = sorted(contestants, key=lambda contestant: contestant.score, reverse=True)
        counter = Counter(contestant.name for contestant in contestants)
        print(f"Generation {generation + 1}")
        payouts = dict(functools.reduce(operator.add, map(Counter, [{contestant.name: contestant.score} for contestant in contestants])))
        percent_per_type = {k: v for d in [{i: counter[i] / len(contestants)} for i in counter] for k, v in d.items()}
        payouts_per_type = {k: v for d in [{payout: payouts[payout] / counter[payout]} for payout in payouts] for k, v in d.items()}
        payouts["Total"] = sum(payouts.values())
        print(f"Payoff per type: {payouts}")
        print(f"Population percentage per type: {percent_per_type}")
        print(f"Average payoff per type: {payouts_per_type}")
        del payouts["Total"]
        total_payout_data["generation"].append(generation + 1), population_percent_data["generation"].append(generation + 1), average_payout_data["generation"].append(generation + 1)

        for player in total_payout_data["players"]:
            try:
                total_payout_data["players"][player].append(payouts[player])
                population_percent_data["players"][player].append(percent_per_type[player])
                average_payout_data["players"][player].append(payouts_per_type[player])
            except KeyError:
                total_payout_data["players"][player].append(0)
                population_percent_data["players"][player].append(0)
                average_payout_data["players"][player].append(0)
        contestants = new_game(contestants, p)

    total_payout_fig = plt.figure()
    markers = ["^", "o", "s", "X"]
    for contestant in total_payout_data["players"]:
        plt.plot(total_payout_data["generation"], total_payout_data["players"][contestant], label=contestant, marker=markers.pop(), markerfacecolor='blue', markersize=12, color='skyblue', linewidth=4)
    plt.legend()
    plt.title(f"Total Payouts per Player")
    plt.xlabel("Generation")
    plt.ylabel("Total Payouts")
    total_payout_fig.savefig(f"programming_assignment_2/data/q3_{p}_total_payouts.png")

    population_percent_fig = plt.figure()
    markers = ["^", "o", "s", "X"]
    for contestant in population_percent_data["players"]:
        plt.plot(population_percent_data["generation"], population_percent_data["players"][contestant], label=contestant, marker=markers.pop(), markerfacecolor='blue', markersize=12, color='skyblue', linewidth=4)
    plt.legend()
    plt.title(f"Population per Player")
    plt.xlabel("Generation")
    plt.ylabel("Percent")
    population_percent_fig.savefig(f"programming_assignment_2/data/q3_{p}_population_percents.png")

    average_payout_fig = plt.figure()
    markers = ["^", "o", "s", "X"]
    for contestant in average_payout_data["players"]:
        plt.plot(average_payout_data["generation"], average_payout_data["players"][contestant], label=contestant, marker=markers.pop(), markerfacecolor='blue', markersize=12, color='skyblue', linewidth=4)
    plt.legend()
    plt.title(f"Average Payouts per Player")
    plt.xlabel("Generation")
    plt.ylabel("Average Payouts")
    average_payout_fig.savefig(f"programming_assignment_2/data/q3_{p}_average_payouts.png")


def new_game(contestants, p) -> list:
    players_to_move = int(len(contestants) * p / 100)
    contestants = contestants[:-players_to_move] + contestants[:players_to_move]
    for player in contestants:
        player.reset()
    return contestants


if __name__ == "__main__":
    for x in range(20, 100, 20):
        play_game(100, 5, x, 20)
