from .players import *

strategies = (
    Defector,
    Cooperator,
    Grudger,
    TitForTat,
)
